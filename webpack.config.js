/**
@description Basic webpack settings for html, js and scss project
@author Ian Reid Langevin
*/

/* Path */
const path = require('path');
/* Plugins */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

/*
Note : the mode is pass in the CLI command in package.json
This project used default webpack settings based on each mode
https://webpack.js.org/configuration/mode/#mode-development
*/
module.exports = {
  //entry
  entry: {
    app: ['./src/app.js']
  },
  // output
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, 'public'),
  },
  // server
  devServer: {
    contentBase: path.join(__dirname, 'src'),
    watchContentBase: true,
    port: 8080,
    watchOptions: {
      ignored: /node_modules/,
    },
  },
  // module
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [ MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader' ]
      }
    ]
  },
  // plugins
  plugins: [
    // create a css file separate from JS
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
    // empty folder before creating js and scss files on build
    new CleanWebpackPlugin(),
    // inject script and scss into html
    new HtmlWebpackPlugin({
      meta: {viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'},
      template: path.resolve(__dirname, "src", "index.html")
    }),
  ],
};
