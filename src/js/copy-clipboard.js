/**
  @function
  @description - copy to clipboard
  @author Ian Reid Langevin
*/

const copyCode = async () => {
  event.stopPropagation()

  const text = document.getElementById("srcset-result")
  let messageText

  navigator.clipboard.writeText(text.innerText)
    .then(() => {
      messageText = "Copié avec succès!"
      showMessage(messageText)
    }, () => { // error
      messageText = "Impossible de copier le texte, essayez manuellement!"
      showMessage(messageText)
    })
}

/**
  @function
  @description - show message
  @author Ian Reid Langevin
*/

function showMessage (message) {
  const messageBox = document.getElementById("message-box")
  const codeBlockHeader = document.getElementById("codeblock-header")
  // inject message and add classes
  messageBox.innerHTML = message
  codeBlockHeader.classList.add("codeblock__header--message")
  messageBox.classList.add("message-box--visible")
  // make element disappears after 2000 ms
  setTimeout(() => {
    codeBlockHeader.classList.remove("codeblock__header--message")
    messageBox.classList.remove("message-box--visible")
  }, 2000)
}

/**
  Attach listener
*/
const copyButton = document.getElementById("copy-button")
copyButton.addEventListener("click", copyCode)
