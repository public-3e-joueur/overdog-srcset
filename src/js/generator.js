
/**
  Main function
*/
function handleEvent (event) {
  event.preventDefault()

  /**
    @constant - Bases values
    @description - Values used to find a match for the image sizes and the breakpoints.
  */

  // breakpoints
  const breakpointsNumbers = {
    xxxl: 1600,
    xxl: 1400,
    xl: 1200,
    lg: 1024,
    md: 768,
    sm: 576,
    initial: 320 // TODO ian: maybe too low - no mobile still using 320px without 2x
  }

  // image formats
  const imageFormats = [
    1920,
    1600,
    1440,
    1280,
    1024,
    960,
    768,
    640,
    480,
    320,
    256,
    200,
    160
  ]

  // ----------------------------------------------o

  /**
    @method - Validation and errors
    @description - Add border and return on error
  */

  function abortIfValidateFails () {
    let abort = false
    let inputMaxValue = 100
    let unitLabel = "pourcentage ou vw"
    // if unit is pixels or percentage
    if (unitIsPixels) {
      inputMaxValue = 1920
      unitLabel = "pixels"
    }

    srcSetInputs.forEach((item, index) => {
      const messageDiv = item.nextElementSibling
      const value = parseInt(item.value, 10)

      if (index === 0 && (value === 0 || item.value.length === 0)) {
        if (event.type === "submit") { // do not show error message on checkbox change - only on submit
          item.classList.add("error")
          messageDiv.innerHTML = "Ce champ doit être rempli et sa valeur plus grande que zéro"
        }
        abort = true

      // else if, if input is filled - validate that the value is bigger than zero but less than the inputMax
      } else if (item.value.length > 0) {
        if (value > inputMaxValue || value === 0) {
          let errorMessage = `La valeur maximale en ${unitLabel} est ${inputMaxValue}`
          if (value === 0) errorMessage = "La valeur doit être plus grande que zéro"
          messageDiv.innerHTML = errorMessage
          item.classList.add("error")
          abort = true
        } else {
          item.classList.remove("error")
          messageDiv.innerHTML = ""
        }
      // reset style for input element
      } else {
        item.classList.remove("error")
        messageDiv.innerHTML = ""
      }
    })

    // return abort value to end function
    return abort
  }

  // ----------------------------------------------o

  /**
    @method - Get lazy and decoding attribute
    @description - Set the lazy attribute based on the checkbox
    @author Ian Reid Langevin
  */

  function returnCheckedRadio (radioName) {
    const radiosButtons = document.getElementsByName(radioName)
    let checkedValue
    radiosButtons.forEach(radio => {
      if (radio.checked === true) checkedValue = radio.value
    })
    return checkedValue
  }

  // ----------------------------------------------o

  /**
    @method - Function to return checkbox value
    @author Ian Reid Langevin
  */

  function returnCheckboxValue (checkboxId, defaultValue, output) {
    const checkboxButton = document.getElementById(checkboxId)
    let valueString = defaultValue
    if (checkboxButton.checked) valueString = output
    return valueString
  }

  // ----------------------------------------------o

  /**
    @method - Function to check if the user want the value in pixel (fixed sizes)
    @author Ian Reid Langevin
  */

  function getUnits () {
    const checkbox = document.getElementById("pixel-size")
    let unitsAbbr = "vw"
    let unitIsPixels = false
    if (checkbox.checked) {
      unitsAbbr = "px"
      unitIsPixels = true
    }
    return { unitsAbbr, unitIsPixels }
  }

  // ----------------------------------------------o

  /**
    @method
    @description - Choose the closest image in the imageFormats array
    @author Gavin de Ste Croix
  */

  function findClosestSize (itemValue) {
    const closest = imageFormats.reduce((a, b) => {
      const aDiff = Math.abs(a - itemValue)
      const bDiff = Math.abs(b - itemValue)

      if (aDiff === bDiff) {
        // Choose largest vs smallest (> vs <)
        return a > b ? a : b
      } else {
        return bDiff < aDiff ? b : a
      }
    })
    return closest
  }

  // ----------------------------------------------o

  /**
    @method - mergeInputValues
    @description - Put the initial input value in a array - used in setSrcSet function
    @author Ian Reid Langevin
  */

  function mergeInputValues (srcSetInputs) {
    const inputsValuesArray = []

    srcSetInputs.forEach(item => {
      inputsValuesArray.push(item.value)
    })
    return inputsValuesArray
  }

  // ----------------------------------------------o

  /**
    @method - Set srcset attributes values
    @description - Get the values for the src-set attribute
    @author Ian Reid Langevin
  */

  function setSrcSet (srcSetInputs) {
    // put initial values in a array
    const valuesArray = mergeInputValues(srcSetInputs)
    // set arrays and get dom elements
    const srcSetArray = []
    // lg size image will be used for fallback
    let fallbackSrcSize

    // loop through the objects
    srcSetInputs.forEach((item, index) => {
      let itemValue

      if (item.value > 0) {
        itemValue = item.value
      } else {
        // else get all the value in the array bigger than zero on smaller breakpoint than current (with the index check)
        const smallerItem = valuesArray.filter((value, valueIndex) => value > 0 && index > valueIndex).slice(-1)
        // reduce to one result if pixel - otherwise, the findClosestSize will return one result
        itemValue = (unitIsPixels) ? smallerItem.reduce((a, b) => { return a.concat(b) }) : smallerItem
      }
      // get the width base on the breakpoint and get the closer value in the imageFormats array
      if (!unitIsPixels) {
        itemValue = (itemValue / 100) * breakpointsNumbers[item.name]
        itemValue = findClosestSize(itemValue)
      }
      // push it in the array if do not already exist
      if (srcSetArray.includes(itemValue) === false) srcSetArray.push(itemValue)
      // Use the value from lg for fallback base src -> for non-support srcset browsers
      if (item.name === "lg") fallbackSrcSize = itemValue
    })
    // if supersize is checked, create a - false - 1.2x bigger image than the bigger
    if (superHighImage === true) {
      const arrayBiggerImg = Math.max(...srcSetArray)
      let oversizeImage
      // if unit is pixel - do not find a match in image array - just 1.3x the bigger
      (!unitIsPixels) ? oversizeImage = findClosestSize(arrayBiggerImg * 1.3) : oversizeImage = arrayBiggerImg * 1.3
      if (srcSetArray.includes(oversizeImage) === false) srcSetArray.push(oversizeImage)
    }
    // sort the array of numbers
    const srcSetArraySorted = srcSetArray.sort((a, b) => a - b)
    // return values
    return { srcSetArraySorted, fallbackSrcSize }
  }

  // ----------------------------------------------o

  /**
    @method - mergeSrcSetString
    @description - Merge the srcSet value into a template string literal
    @author Ian Reid Langevin
  */

  function mergeSrcSetString (srcSetArraySorted) {
    const srcSetStringArray = []

    srcSetArraySorted.forEach(item => {
      const itemSizeString = `{{ photo.url }}&w=${item}&auto=${autoCompressOption}format ${item}w`
      srcSetStringArray.push(itemSizeString)
    })
    // join and indent
    const srcSetStringJoined = srcSetStringArray.join(", \n                 ")
    // return values
    return srcSetStringJoined
  }

  // ----------------------------------------------o

  /**
    @method - setSizes
    @description - set the sizes attributes based on breakpoints values
    @author Ian Reid Langevin
  */

  function setSizes (srcSetInputs) {
    const sizesArray = []

    srcSetInputs.forEach(item => {
      let itemSize
      if (item.name === "initial") {
        itemSize = `${item.value}${unitsAbbr}`
        sizesArray.push(itemSize)
      } else if (item.value) {
        itemSize = `(min-width: ${breakpointsNumbers[item.name]}px) ${item.value}${unitsAbbr}`
        sizesArray.push(itemSize)
      }
    })
    const sizesJoined = sizesArray.reverse().join(", ")
    return sizesJoined
  }

  // ----------------------------------------------o

  /**
    @constant
    @description - Execute all the methods here
  */

  // get input fields values
  const srcSetInputs = document.querySelectorAll("#srcset-form input")
  // get the unit for the output (needed for validation)
  const { unitsAbbr, unitIsPixels } = getUnits()
  // validation of the input size fields and show code div only if validation is ok
  if (abortIfValidateFails() === true) return
  document.getElementById("codeblock").style.display = "block"
  // get sidebar options
  const lazyAttribute = returnCheckedRadio("lazy")
  const decodingAttribute = returnCheckedRadio("decoding")
  const wrapperClass = returnCheckedRadio("wrapper-class")
  const dataPrefixOption = returnCheckboxValue("data-prefix", "", "data-")
  const autoCompressOption = returnCheckboxValue("auto-compress", "", "compress,")
  const superHighImage = returnCheckboxValue("oversize-image", false, true)
  // function with returned values as parameters
  const { srcSetArraySorted, fallbackSrcSize } = setSrcSet(srcSetInputs)
  const srcSetJoined = mergeSrcSetString(srcSetArraySorted)
  const sizesString = setSizes(srcSetInputs)

  // ----------------------------------------------o

  /**
    @constant - Template literals
    @description - Output the html template literals for the img tag
  */
  let heightWidthBlock = ""

  if (wrapperClass === "lazy-img") {
    heightWidthBlock = `
    height="{{ photo.height }}"
    width="{{ photo.width }}"`
  }

  const html =
  `<div class="${wrapperClass} votre-classe-de-style">
  <img ${heightWidthBlock}
    loading="${lazyAttribute}"
    decoding="${decodingAttribute}"
    ${dataPrefixOption}srcset="${srcSetJoined}"
    sizes="${sizesString}"
    ${dataPrefixOption}src="{{ photo.url }}&w=${fallbackSrcSize}"
    alt="{{ photo.title }}"
  />
</div>`

  document.getElementById("srcset-result").textContent = html
} // event

// ----------------------------------------------o

// run main function
const srcsetForm = document.getElementById("srcset-form")
const settingsForm = document.getElementById("attributes-options")

srcsetForm.addEventListener("submit", handleEvent)
settingsForm.addEventListener("change", handleEvent)
